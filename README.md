Comunidades Paraenses de TI e Inovação
======================================

As comunidades paraenses de tecnologia e afins estão bombando na vida real e nos grupos no Telegram e WhatsApp! Participe você também. Esse repositório contém uma lista organizada das comunidades (e seus meios de comunicação) por temas na tecnologia.

Inspiração no [Repositório de Canais e Grupos Brasileiros de TI ](https://github.com/alexmoreno/telegram-br). Conheças várias outras comunidades lá também.

Para realizar pedidos de novas comunidades:
1. Crie um pull request para adicionar novas comunidades;
2. Retire o :triangular_flag_on_post: das comunidades com esse ícone e coloque nas novas comunidades adicionadas;
3. Avise no grupo do [ParaLivre no Telegram](http://t.me/paralivre) a alteração.
**OBS**: Caso não saiba usar o GitLab, basta pedir para que sua comunidade seja adicionada aqui no grupo do telegram do [ParaLivre](http://t.me/paralivre). 

# Temas gerais de TI e Empreendedorismo
- **Açaí Valley**: Associação Paraense de Tecnologia e Inovação
  - [Site](http://www.acaivalley.org)
  - [Facebook](https://www.facebook.com/acaivalley)
  - [Instagram](https://www.instagram.com/acaivalley)
  
# Eventos
- **Agenda TI Pará**: Agenda de Eventos de TI no Pará
  - [Site](https://agendatipa.paralivre.org)
  - [Calendário](https://agendatipa.paralivre.org/calendario)  
  - [Telegram](https://t.me/agendatipara)  
  - [Twitter](https://www.twitter.com/agendatipara)  
  - [Facebook](https://www.facebook.com/agendatipara)
  - [Instagram](https://www.instagram.com/agendatipara)

# Software Livre e Open Source
- **ParaLivre**: Comunidade Paraense de Software Livre
  - [Site](https://www.paralivre.org)
  - [Telegram](https://t.me/paralivre)  
  - [Twitter](http://www.twitter.com/paralivre_)
  - [Instagram](https://www.instagram.com/paralivre)
 
- **Centro de Competência em Software Livre - UFPA**
  - [Site](http://ccsl.ufpa.br)
 
- **Linux Pai d'Égua**: Linux em Belém do Pará
  - [Yahoo Grupo](https://br.groups.yahoo.com/neo/groups/linuxpaidegua)
  
# Linguagens de Programação
- **GruPy Belém**: Comunidade de Python de Belém
  - [WhatsApp](https://chat.whatsapp.com/K7cbnG8f4UIICMUHWOwA5h)
  
- **PHP Pará**: Elephants Pará
  - [Site](https://phppa.org/)
  - [Telegram](https://t.me/PHPPA)
  - [Twitter](https://twitter.com/phppara)
  - [Facebook](https://www.facebook.com/elephants.para)
  
- **Java Belém**: Galera do Java Belém PA
  - [WhatsApp](https://chat.whatsapp.com/KTfRmaqPRqJ4Oyn5jENfnn)
  - [Telegram](https://t.me/joinchat/LwNStBerdCoTZZIlrDjIPA)
 
# Programação e desenvolvimento aplicado e ágil
- **Vue.js Norte**: Comunidade de desenvolvedores Vue.js
  - [GitHub](https://github.com/vuejs-norte)
  - [GitHub.io](https://vuejs-norte.github.io)
  - [Telegram](https://t.me/joinchat/KjIVwE5XY11h7evjXTwU0A)
  - [WhatsApp](https://chat.whatsapp.com/LiHQUNP4k7tBDLa5oXp2se)
  - [Facebook](https://www.facebook.com/vuejsnorte)
  - [Instagram](https://www.instagram.com/vuejsnorte)
  - [Twitter](https://twitter.com/NorteVue)
  - [Meetup](https://www.meetup.com/pt-BR/Vue-js-Norte)
  
- **Tá Safo**: Tecnologias Abertas com Software Ágil, Fácil e Organizado
  - [Site](https://tasafo.org)
  - [Slack](tasafo.slack.com)
  - [Twitter](https://twitter.com/tasafo)
  - [Facebook](https://www.facebook.com/tasafo.comunidade)
  - [Instagram](https://www.instagram.com/comunidadetasafo)
  
# Desenvolvimento de Jogos
- **Bel Jogos**: Grupo de entusiastas, hobbistas, apreciadores e desenvolvedores de jogos eletrônicos em Belém
  - [Grupo Yahoo](https://br.groups.yahoo.com/neo/groups/beljogos)
  - [Twitter](https://twitter.com/beljogos)
  - [Facebook](https://www.facebook.com/BeljogosPA)
  - [Youtube](https://www.youtube.com/channel/UCa0FvmBQz0JiActsGs5r5-A)  
  
- **GameDevsPA**: Grupo multidisciplinar formado por diversos profissionais que trabalham em conjunto para o fortalecimento da indústria de jogos no estado do Pará.
  - [Instagram](https://www.instagram.com/gamedevspa)
  - [Facebook Fan Page](http://facebook.com/gamedevspa)
  - [Facebook Grupo](https://www.facebook.com/groups/117244738954945/)  
  - [Youtube](https://www.youtube.com/channel/UCOzB-hyIPS_DanMritkuHKA)  
  
# DEVOPS
- **DevOps Parauapebas**: Grupo de Entusiastas, Estudantes e Profissionais de TI de Parauapebas
  - [WhatsApp](https://chat.whatsapp.com/7XfTiu53icSKKbANTQnMGH)
  - [Telegram](https://t.me/joinchat/A-G57xd_fjAnQwOzV_sdeQ)
  - [Instagram](https://instagram.com/devopspbs)
  - [GitLab](https://gitlab.com/devopspbs)
  - [MeetUp](https://meetup.com/devopspbs)    
  - [Site](https://devopspbs.org)    

# Maker
- **Arduino Parauapebas**: Grupo que promove eventos, projetos, grupo de estudo e todas as outras possibilidades envolvendo o Arduino e outros embarcados
  - [WhatsApp](https://chat.whatsapp.com/invite/KADdIGqT9KT5i4BOoJcg6T)
  - [Youtube](https://www.youtube.com/channel/UCDSOUrnRptHkP9Atv-zO_qg)
  - [Facebook](http://www.facebook.com/ArduinoParauapebas)

# Banco de Dados, Data Platform
- **SQL Norte**: O grupo compartilha conhecimento sobre dados em eventos, promove o networking na Comunidade do Norte do País
  - [WhatsApp](https://chat.whatsapp.com/EzD5z7bpXc41fbW0TCw31Q)
  - [WhatsApp](https://chat.whatsapp.com/BCWb6O09jPA1VWZGgIygG7)  
  - [Telegram](https://t.me/joinchat/H3xPKxFX2iO8wKMyU55kiQ)
  - [Facebook](https://www.facebook.com/sqlnorte)
  - [Instagram](https://www.instagram.com/sqlnorte)
  - [Twitter](https://twitter.com/sqlnorte)
  - [Youtube](https://www.youtube.com/channel/UCBvWwvJQY0JaoHC2YgJrJ2Q)
  - [Linkedin](https://www.linkedin.com/company/sqlnorte)
  - [Meetup](https://www.meetup.com/pt-BR/SQL-Norte)
     
- **Power BI Carajás (SQL Carajás)**: Grupo de usuários do Power BI em Parauapebas e Região de Carajás
  - [WhatsApp](https://chat.whatsapp.com/invite/GokJPr6PyRy4NbW7k1mzOL)
  - [MeetUp](https://www.meetup.com/PowerBI-Carajas)    

# Promoção e reforço da participação feminina na tecnologia
- **PyLadies Belém**: É um grupo internacional, com foco em ajudar mais mulheres a se tornarem participantes ativas e líderes da comunidade de código aberto Python 
  - [Facebook](https://www.facebook.com/pyladiesbelem)
  - [Instagram](https://www.instagram.com/pyladiesbelem)
    
- **Rails Girls Belém**: É uma comunidade para que as mulheres entendam a tecnologia e construam suas idéias.
  - [Facebook](https://www.facebook.com/railsgirlsbelem)
  - [Instagram](https://www.instagram.com/railsgirlsbelem)
  - [Site](http://railsgirls.com/belem)  
  
- **Paragobyte Girls**: O grupo tem como objetivo realizar ações que estimulem a participação e a formação de mulheres nas áreas de ciência e tecnologia
  - [Facebook](https://www.facebook.com/paragobytegirls)
  - [Site](http://meninas.sbc.org.br/index.php/portfolio/paragobytegirls/)
  
- **DevOps Girls Parauapebas**
  - [WhatsApp](https://chat.whatsapp.com/L2L2emg0pCy5K1xoyP6IYE)
     
- **Manas Digitais**: Tem como objetivo a realização de práticas de caráter motivacional e informativo para promover a carreira na computação e áreas tecnológicas
  - [Facebook](https://www.facebook.com/manasdigitais)
  - [Instagram](https://www.instagram.com/manasdigitais)
  - [Site](http://meninas.sbc.org.br/index.php/portfolio/manas-digitais/)

- **Django Girls Belém**
  - [Facebook](https://www.facebook.com/DjangoGirlsBelem)
  - [Instagram](https://www.instagram.com/djangogirlsbelem)
  - [Site](https://djangogirls.org/belem)
 
- **Meninas da Geotecnologia**: Projeto de Extensão do IFPA campus Castanhal, chancelado pela Sociedade Brasileira de Computação.
  - [Facebook](https://www.facebook.com/meninasdageo)
  - [Instagram](https://www.instagram.com/meninasdageo)
  - [Site](http://meninas.sbc.org.br/index.php/portfolio/meninas-da-geotecnologia/)
 
# Capítulos locais de organizações internacionais
- **GDG Belém**: Somos o Google Developers Group oficial de Belém
  - [MeetUp](https://www.meetup.com/pt-BR/gdgbelemio)
  - [Facebook](https://www.facebook.com/GDGBelem)  
  - [Instagram](https://www.instagram.com/gdgbelemoficial)
  - [Twitter](https://twitter.com/GDGBelem)
  - [GitHub.io](hhttps://gdgbelem.github.io/home/)
  - [Slack](https://gdgbelem.herokuapp.com)
  
- **Legal Hackers Belém**: Primeiro capítulo paraense do movimento global Legal Hackers! Conectando o Direito, Tecnologia e Inovação.
  - [Instagram](https://www.instagram.com/legalhackersbelem)
  - [WhatsApp](https://chat.whatsapp.com/LLyTAtzoQ9M3TyuwsZo1Rg)
  - [Facebook](https://www.facebook.com/belemlegalhackers)
  - [Twitter](https://twitter.com/legalhackersbel)        

- **PMI Branch Pará**: É a ramificação do PMI (Project Management Institute) Amazônia Chapter no Estado do Pará.
  - [Site](http://pmiam.org/para-branch/)
  - [Instagram](https://www.instagram.com/pmiamoficial)
  - [Facebook](https://www.facebook.com/bpmiam)
  - [Twitter](https://twitter.com/pmiam)        

# Associações Empresariais
- **ParaTic**: Associação das Empresas Paraenses de Software e Tecnologia da Informação e Comunicação
  - [Site](http://www.paratic.com.br)
  - [Facebook](https://www.facebook.com/paraticbrasil)  
  - [Instagram](https://www.instagram.com/associacao_paratic)
    
